﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class TimeSlotRepository : ITimeSlotRepository
    {
        private readonly AppDbContext _appDbContext;
        public TimeSlotRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<TimeSlot> GetAllTimeSlots()
        {
            return _appDbContext.TimeSlots;
        }

        public TimeSlot GetTimeSlotById(int id)
        {
            return _appDbContext.TimeSlots.FirstOrDefault(b => b.Id == id);
        }
    }
}
