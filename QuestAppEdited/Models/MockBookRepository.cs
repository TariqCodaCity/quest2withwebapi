﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class MockBookRepository : IBookRepository
    {
        private List<Book> _books;
        public MockBookRepository()
        {
            if(_books == null)
            {
                InitializeBooks();
            }
        }
        public void InitializeBooks()
        {
            _books = new List<Book>
            {
                    new Book{ BookingId=1, Name="Tariq", Mobile="0798482345", TreatmentId=1, TimeSlotId=1, Note="I want doctor number 1 ", Email="tariq@coda-city.com",BookedDate=DateTime.Parse("01/02/2018"),IsDeleted=true},
                    new Book{ BookingId=2, Name="Aalaa", Mobile="0790107576", TreatmentId=2, TimeSlotId=2, Note="I want doctor number 2 ", Email="Aalaa@coda-city.com",BookedDate=DateTime.Parse("05/02/2018"),IsDeleted=true},
                    new Book{ BookingId=3, Name="Seraa", Mobile="0798482345", TreatmentId=3, TimeSlotId=3, Note="I want doctor number 3 ", Email="seraa@coda-city.com",BookedDate=DateTime.Parse("10/03/2018"),IsDeleted=true},
                    new Book{ BookingId=4, Name="Ahmad", Mobile="0798485647", TreatmentId=4, TimeSlotId=4, Note="I want doctor number 4 ", Email="ahmad@coda-city.com",BookedDate=DateTime.Parse("11/11/2018"),IsDeleted=true},
                    new Book{ BookingId=5, Name="Muhammad", Mobile="0744523987", TreatmentId=5, TimeSlotId=5, Note="I want doctor number 5 ", Email="muhammad@coda-city.com",BookedDate=DateTime.Parse("05/12/2018"),IsDeleted=true},
                    new Book{ BookingId=6, Name="Sumaia", Mobile="0777856954", TreatmentId=6, TimeSlotId=6, Note="I want doctor number 6 ", Email="sumaia@coda-city.com",BookedDate=DateTime.Parse("07/04/2018"),IsDeleted=true},
                    new Book{ BookingId=7, Name="Hadeel", Mobile="0784856325", TreatmentId=7, TimeSlotId=7, Note="I want doctor number 7 ", Email="hadeel@coda-city.com",BookedDate=DateTime.Parse("09/02/2018"),IsDeleted=true},
                    new Book{ BookingId=8, Name="Batool", Mobile="0765123456", TreatmentId=8, TimeSlotId=8, Note="I want doctor number 8 ", Email="batool@coda-city.com",BookedDate=DateTime.Parse("06/06/2018"),IsDeleted=true},
                    new Book{ BookingId=9, Name="Fatima", Mobile="0778412578", TreatmentId=9, TimeSlotId=9, Note="I want doctor number 9 ", Email="fatima@coda-city.com",BookedDate=DateTime.Parse("04/04/2018"),IsDeleted=true},
                    new Book{ BookingId=10, Name="Mahmoud", Mobile="0788457874", TreatmentId=10, TimeSlotId=10, Note="I want doctor number 10 ", Email="mahmoud@coda-city.com",BookedDate=DateTime.Parse("03/05/2018"),IsDeleted=true}
            };
        }
        public IEnumerable<Book> GetAllBooks()
        {
            return _books;
        }

        public Book GetBookById(int BookingId)
        {
            return _books.FirstOrDefault(b => b.BookingId == BookingId);
        }
    }
}
*/