﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class TreatmentRepository : ITreatmentRepository
    {
        private readonly AppDbContext _appDbContext;
        public TreatmentRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Treatment> GetAllTreatments()
        {
            return _appDbContext.Treatments;
        }

        public Treatment GetTreatmentById(int id)
        {
            return _appDbContext.Treatments.FirstOrDefault(b => b.Id == id);
        }
    }
}
