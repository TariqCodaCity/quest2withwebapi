﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public interface ITimeSlotRepository
    {
        IEnumerable<TimeSlot> GetAllTimeSlots();
        TimeSlot GetTimeSlotById(int id);
    }
}
