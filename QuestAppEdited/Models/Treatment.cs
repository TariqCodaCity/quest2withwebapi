﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class Treatment
    {
        [Required, Key]
        public int Id { get; set; }
        public string TreatmentTitle { get; set; }
        public string TreatmentDescription { get; set; }
        public List<Book> Books { get; set; }
    }
}
