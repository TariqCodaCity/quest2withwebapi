﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public interface ITreatmentRepository
    {
        IEnumerable<Treatment> GetAllTreatments();
        Treatment GetTreatmentById(int id);
    }
}
