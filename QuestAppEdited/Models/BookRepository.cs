﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public class BookRepository:IBookRepository
    {
        private readonly AppDbContext _appDbContext;
        public BookRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _appDbContext.Books;
        }

        public Book GetBookById(int BookingId)
        {
            return _appDbContext.Books.FirstOrDefault(b => b.BookingId == BookingId);
        }

        public Book GetBookByEmail(string mail)
        {
            return _appDbContext.Books.FirstOrDefault(b => b.Email == mail);
        }


        public void AddBook(Book book)
        {
             _appDbContext.Books.Add(book);
            _appDbContext.SaveChanges();
        }

        public void UpdateBook(Book book)
        {
            _appDbContext.Books.UpdateRange(book);
            _appDbContext.SaveChanges();
        }



        public void DeleteBook(Book book)
        {
            _appDbContext.Books.UpdateRange(book);
            _appDbContext.SaveChanges();
        }
    }
}
