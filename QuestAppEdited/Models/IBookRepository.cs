﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestAppEdited.Models
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAllBooks();
        Book GetBookById(int BookingId);
        void AddBook(Book book);
        void UpdateBook(Book book);
        void DeleteBook(Book book);
        Book GetBookByEmail(string email);
    }
}
