﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using QuestAppEdited.Models;
using QuestAppEdited.ViewModels;
using Microsoft.Extensions.Logging;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuestAppEdited.Controllers
{
    public class HomeController : Controller

    {
        private AppDbContext _appDbContext;
        private readonly IBookRepository _bookRepository;
        private readonly ITreatmentRepository _treatmentRepository;
        private readonly ITimeSlotRepository _timeSlotRepository;

        public HomeController(IBookRepository bookRepository, AppDbContext appDbContext, ITreatmentRepository treatmentRepository, ITimeSlotRepository timeSlotRepository)
        {
            _appDbContext = appDbContext;
            _bookRepository = bookRepository;
            _treatmentRepository = treatmentRepository;
            _timeSlotRepository = timeSlotRepository;
        }


        [HttpGet("api/books")]
        public JsonResult Index()
        {
            var books = _bookRepository.GetAllBooks().OrderBy(b => b.Name).Where(b => b.IsDeleted == false);
            var result = new JsonResult(books);
            result.StatusCode = 200;
            return result;
        }
       
      
            [HttpGet("api/books/search/{searchString}")]
            public IActionResult Index(string searchString)
            {
                var books = _bookRepository.GetAllBooks().Where(b => b.IsDeleted == false).OrderBy(b => b.Name);

                if (searchString != null)
                {
                    var searchedSuccess = books.Where(b => b.Name.Contains(searchString) || b.Mobile.Contains(searchString));


                    return new JsonResult(searchedSuccess);
                }
                else
                {

                    return new JsonResult(books);
                }
            }
      

        [HttpGet("api/books/{id}")]
        public JsonResult Details(int id)
        {

            var book = _bookRepository.GetBookById(id);


            if (book == null)
                return new JsonResult("no book");
            else
            {
               var treatment = _treatmentRepository.GetTreatmentById(book.TreatmentId);
               var timeslot = _timeSlotRepository.GetTimeSlotById(book.BookingId);


              var result = new {book =book, treatmentTitle = treatment.TreatmentTitle, timeslotTitle = timeslot.TimeSlotTitle };
                return new JsonResult(result);
            }


        }
        [HttpGet("api/delete/{id}")]
        public IActionResult DeleteBook(int id)
        {
            var book = _bookRepository.GetBookById(id);
            if (book == null)
                return NotFound();
            return Ok(book);
        }

        [HttpPost("api/delete/{id}")]
        public IActionResult DeleteBook([FromBody] Book book)
        {

                _bookRepository.UpdateBook(book);
                return RedirectToAction("Index");
    
        }




        [HttpGet("api/update/{id}")]
        public IActionResult UpdateBook(int id)
        {
            var book = _bookRepository.GetBookById(id);
            if (book == null)
                return NotFound();
            return Ok(book);
        }
        [HttpPost("api/update/{id}")]
        public IActionResult UpdateBook([FromBody] Book book)
        {
            _bookRepository.UpdateBook(book);
            return RedirectToAction("Index");
        }

        [HttpGet("api/addbook")]
        public IActionResult AddBook() {

            var timeslot = _timeSlotRepository.GetAllTimeSlots();
            var treatment = _treatmentRepository.GetAllTreatments();


                var result = new { timeSlot = timeslot, Treatment = treatment};
                return Json(result);
        }
       
        [HttpPost("api/addbook")]
        public IActionResult AddBook([FromBody] Book book)
        {
            if(book == null)
            {
                return new JsonResult("Error the book is empty");
            }
            _bookRepository.AddBook(book);
            return RedirectToAction("Index");
        }
    }
}
